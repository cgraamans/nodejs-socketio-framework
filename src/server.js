import {Service} from './libs/service.js';
import {Options} from './libs/options.js';

console.log(Options);

const svc = new Service(Options);

svc.init()
	.then(()=>{
		svc.io.on('connection',socket=>{
			console.log('user connected');
		});
		console.log('server ready');
	})
	.catch(e=>{
		console.log(e);
	});