'use strict';

import DB from './db';

export class Service {

	constructor(options){

		if(options) this.options = options;

	}

	init() {

		return new Promise((resolve,reject)=>{

			try {

				if(this.options && this.options.DB) this.DB = new DB(this.options.DB);
				if(this.options && this.options.SOCKET) {

					this.io = require('socket.io')(this.options.SOCKET.port,this.options.SOCKET.settings);			
					if(this.options.SOCKET.redis.active) {

						let redisAdapter = require('socket.io-redis');
						this.io.adapter(redisAdapter({ host: this.options.SOCKET.redis.host, port: this.options.SOCKET.redis.port }));

					}

				}
				resolve();

			} catch(e){

				reject(e);

			}

		});

	}

	patchSocket(socket){

		return new Promise((resolve,reject)=>{



		});


	}

};